package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

var (
	c = flag.Bool("c", false, "find number of a string occurrences")
	d = flag.Bool("d", false, "show repeated lines")
	u = flag.Bool("u", false, "show unique lines")
)

func getData(pathInputFile string) ([]string, error) {
	var input io.Reader
	if filename := pathInputFile; filename != "" {
		f, err := os.Open(filename)
		if err != nil {
			return nil, err
		}
		defer f.Close()
		input = f
	} else {
		input = os.Stdin
	}
	buf := bufio.NewScanner(input)
	var data []string
	for {
		if !buf.Scan() {
			break
		}
		data = append(data, strings.Split(buf.Text(), `\n`)...)
	}
	if err := buf.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "error reading: err:", err)
		return data, err
	}
	return data, nil
}

func outputData(data []string, pathOutputFile string) error {
	var output io.Writer
	if filename := pathOutputFile; filename != "" {
		f, err := os.Create(pathOutputFile)
		if err != nil {
			return err
		}
		defer f.Close()
		output = f
	} else {
		output = os.Stdout
	}
	writer := bufio.NewWriter(output)
	for i := range data {
		_, err := writer.WriteString(data[i]) // запись строки
		if err != nil {
			return err
		}
		_, err = writer.WriteString("\n") // перевод строки
		if err != nil {
			return err
		}
	}
	writer.Flush()
	return nil
}

func stringToMap(input []string) map[string]int {
	var result = make(map[string]int)
	for i := 0; i < len(input); i++ {
		result[input[i]] += 1
	}
	return result
}

func countOccur(input []string) []string {
	data := stringToMap(input)
	var result []string
	for key, value := range data {
		result = append(result, strconv.FormatInt(int64(value), 10)+" "+key)
	}
	return result
}

func showRepeated(input []string) []string {
	data := stringToMap(input)
	var result []string
	for key, value := range data {
		if value > 1 {
			result = append(result, strconv.FormatInt(int64(value), 10)+" "+key)
		}
	}
	return result
}

func showUnique(input []string) []string {
	data := stringToMap(input)
	var result []string
	for key, value := range data {
		if value == 1 {
			result = append(result, strconv.FormatInt(int64(value), 10)+" "+key)
		}
	}
	return result
}

func doSmth(data []string) []string {
	if *c || *d || *u {
		if *c {
			data = countOccur(data)
		}
		if *d {
			data = showRepeated(data)
		}
		if *u {
			data = showUnique(data)
		}
	}
	return data
}

func main() {

	input := flag.String("input_file", "", "path to the input file")
	output := flag.String("output_file", "", "path to the output file")
	flag.Parse()

	data, err := getData(*input)

	if err == nil {
		data = doSmth(data)
		err = outputData(data, *output)
	} else {
		fmt.Errorf("error opening file")
	}

	//fmt.Println(data)

}
